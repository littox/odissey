module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Odyssey Found',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Odyssey Found' },
      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#ffffff' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:400,500,700&amp;subset=cyrillic,cyrillic-ext' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png',  href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', href: '/favicon-16x16.png' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#5bbad5' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#5bbad5' },
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    // proxyHeaders: false
  },
  plugins: [
    '~/plugins/element-ui',
    { src: '~/plugins/scroll-to', ssr: false },
    { src: '~/plugins/js.js', ssr: false },
  ],
  css: [
    '~/node_modules/normalize.css/normalize.css',
    '~/node_modules/element-ui/lib/theme-chalk/reset.css',
    '~/node_modules/element-ui/lib/theme-chalk/container.css',
    '~/node_modules/element-ui/lib/theme-chalk/col.css',
    '~/node_modules/element-ui/lib/theme-chalk/collapse.css',
    '~/node_modules/element-ui/lib/theme-chalk/collapse-item.css',
    '~/node_modules/element-ui/lib/theme-chalk/row.css',
    '~/node_modules/element-ui/lib/theme-chalk/display.css',
    '~/node_modules/element-ui/lib/theme-chalk/dialog.css',
    '~/node_modules/element-ui/lib/theme-chalk/icon.css',
    '~/assets/css/app.less',
  ],
};

