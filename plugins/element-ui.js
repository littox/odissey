import Vue from 'vue';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'

export default () => {
  Vue.prototype.$ELEMENT = { size: 'small', zIndex: 1000, locale };
  Vue.use(ElementUI);
}
