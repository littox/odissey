FROM node:8.14.0-alpine
ENV HOST 0.0.0.0
RUN set -xe \
    && apk add --no-cache bash git openssh yarn \
    && npm install -g npm \
    && git --version && bash --version && ssh -V && npm -v && node -v && yarn -v
EXPOSE 3000
